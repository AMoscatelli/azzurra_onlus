function TermsAndConditions() {

	days = 30;
	myDate = new Date();
	myDate.setTime(myDate.getTime() + (days * 24 * 60 * 60 * 1000));

	document.cookie = 'TermsAndConditions=Accepted; expires='
			+ myDate.toGMTString();
	
	
	//cookie Google Analitycs
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'UA-136439000-1');
	
	$('#modalCookie1').hide();

}

function Decline() {

	days = 1;
	myDate = new Date();
	myDate.setTime(myDate.getTime() + (days * 24 * 60 * 60 * 1000));

	document.cookie = 'TermsAndConditions=NotAccepted; expires='
			+ myDate.toGMTString();

}



$( document ).ready(function() {
if ($.cookie("TermsAndConditions") === "Accepted") {
} else {
	creaModal();
}
});



function creaModal(){
	var html = '<!--Modal: modalCookie-->';
	html += '<div class="modal fade top" id="modalCookie1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="false">';
	html += '<div class="modal-dialog modal-frame modal-top modal-notify modal-info" role="document">';
	html += '    <!--Content-->';
	html += '    <div class="modal-content">';
	html += '     <!--Body-->';
	html += '<div class="modal-body">';
	html += '<div class="row d-flex justify-content-center align-items-center text-center">';

	html += '<p class="pt-3 pr-2">Questo sito o gli strumenti di terze parti in esso integrati fanno uso di cookie necessari per il funzionamento e per il raggiungimento delle finalità descritte nella cookie policy. Per saperne di più o per revocare il consenso relativamente a uno o tutti i cookie, fai riferimento alla <a href="policy.php">cookie policy</a>. Dichiari di accettare l’utilizzo di cookies chiudendo o nascondendo questo banner, proseguendo la navigazione di questa pagina, cliccando un link o continuando a navigare in altro modo</p>';

	html += '<a type="button" class="btn btn-primary" id="accetta">Continua<i class="fas fa-check ml-1"></i></a>';
//	html += '<a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal" id="declina">Declina</a>';
	html += '</div>';
	html += '</div>';
	html += ' </div>';
	html += '<!--/.Content-->';
	html += '</div>';
	html += '</div>';
	html += '<!--Modal: modalCookie-->';
		
	
	$(html).appendTo('body');
	
	$("#accetta").click(function() {
		TermsAndConditions();
	});

	$("#declina").click(function() {
		Decline();
	});
	
	window.onscroll = function (e) 	{
		TermsAndConditions();		
	}
		
	$('#modalCookie1').modal('show');
}
