<?php
require_once 'php/const.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}

.servizi_title{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 260%;
}

.servizi_desc{
	font-family: "Kalam";
    font-weight: 100;
    font-style: normal;
    font-size: 120%;
}

.font_custom{
	font-family: "Kalam";
}


</style>

</head>

<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand navbar_title" href="index.php" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA '84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item"><a class="nav-link navbar_title" href="index.php">Home<span class="sr-only">(current)</span></a></li>
					<li class="nav-item active"><a class="nav-link navbar_title" href="#">Servizi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="attivita.php">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="successi.php">I nostri successi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="chisiamo.php">Chi siamo</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="contatti.php">Contatti</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="policy.php">Privacy policy</a></li>
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
    					<a href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
    						class="nav-link border border-light rounded waves-effect waves-light navbar_title"
    						target="_blank"> 
    						<i class="fab fa-facebook-square mr-2"></i>Seguici su Facebook
    					</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Main layout-->
	<main>
<div class="container" style="margin-top: 130px">

<!-- Projects section v.3 -->
<section>

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center servizi_title">I nostri servizi</h2>
  <!-- Section description -->
  <p class="text-center w-responsive mx-auto mb-5 servizi_desc">AZZURRA '84 si rivolge ad anziani, minori e loro famiglie, persone in difficolt&agrave; (emarginati, detenuti, ex tossicodipendenti, immigrati, giovani a rischio, ecc), portatori di disagi fisici e psichici, oltre che a persone che affrontino momenti critici del loro percorso di vita, realizzando interventi e servizi di prevenzione, cura, assistenza, riabilitazione, anche attraverso l&rsquo;inserimento al lavoro di persone in condizioni di svantaggio.</p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 mb-lg-0 mb-5">
      <!--Image-->
      <img src="images/home/family.jpg" class="img-fluid z-depth-1-half" alt="">
    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3 mt-2">
        <div class="ml-2">
          <h5 class="font-weight-bold mb-3 font_custom">Sostegno a minori e famiglie</h5>
          <p class="grey-text font_custom">"Punti di riferimento per accompagnare i genitori alla crescita propria e dei propri figli"</p>
          <p class="font_custom" align="justify">Facilitare l&rsquo;esercizio di un buon ruolo genitoriale e sostenere i genitori nel far fronte alle difficolt&agrave; della loro vita &egrave; l&rsquo;obiettivo dei servizi per minori e famiglie realizzati in diversi municipi del territorio romano. Studi e ricerche ci confermano che esiste un forte collegamento tra la vita nella famiglie e il tipo di adulti che i bambini della famiglia diventeranno ed il tipo di soddisfazione che le persone provano nella loro vita. Portare avanti una famiglia &egrave; un lavoro duro ed &egrave; necessario poter contare su una serie di aiuti: quello che noi offriamo comprende la consulenza psico-educativa, il sostegno genitoriale, la mediazione familiare, gli interventi di spazio neutro, e una serie di attivit&agrave; di supporto all&rsquo;adozione e all&rsquo;affidamento familiare. </p>
        </div>
      </div>
      <!-- Grid row -->


    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3 mt-2">
        <div class="ml-2">
          <h5 class="font-weight-bold mb-3 font_custom">Inserimento socio-lavorativo</h5>
          <p class="grey-text font_custom">"Arricchire le competenze personali e lavorative"</p>
          <p class="font_custom" align="justify">Garantire il diritto al lavoro di tutti i cittadini, creare percorsi personalizzati finalizzati allo sviluppo di competente lavorative, promuovere l&rsquo;inserimento ed il reinserimento delle persone sono alcune delle azioni che vengono realizzate all&rsquo;interno della cooperativa. </p>
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-5">
      <!--Image-->
      <img src="images/home/inserimento.jpg" class="img-fluid z-depth-1-half" alt="">
    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->
  
  <hr class="my-5">
  
   <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5 mb-lg-0 mb-5">
      <!--Image-->
      <img src="images/home/disabilita.jpg" class="img-fluid z-depth-1-half" alt="">
    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3 mt-2">
        <div class="ml-2">
          <h5 class="font-weight-bold mb-3 font_custom">Sostegno all&rsquo;autonomia di persone con fragilit&agrave;</h5>
          <p class="grey-text font_custom">"Tutelare il diritto ad una buona vita"</p>
          <p class="font_custom" align="justify">Si realizzano una serie di interventi, anche domiciliari,  a favore delle persone anziane e/o con disabilit&agrave; in maniera da offrire risposte, a seconda dei bisogni delle persone e delle loro famiglie, di tipo sia sociosanitario sia sociale. </p>
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row mb-5">

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3 mt-2">
        <div class="ml-2">
          <h5 class="font-weight-bold mb-3 font_custom">Interventi nelle aree delle dipendenze</h5>
          <p class="grey-text font_custom">"Camminare insieme per andare lontano"</p>
          <p class="font_custom" align="justify">Ci occupiamo  a pi&ugrave; livelli di dipendenze patologiche svolgendo attivit&agrave; di:<br>
- PREVENZIONE delle dipendenze comportamentali e delle dipendenze da sostanze, con interventi mirati a sensibilizzare ragazzi e giovani adulti, che forniscano loro strumenti e informazioni per limitare l&rsquo;attuazione di comportamenti a rischio. Il focus degli interventi riguarda l&rsquo;aumento della consapevolezza nei ragazzi delle proprie emozioni e del proprio disagio, nonch&egrave; la capacit&agrave; di esprimerlo in maniera costruttiva all&rsquo;interno di un rapporto con un adulto di riferimento, contenendone cos&igrave; l&rsquo;esternalizzazione attraverso comportamenti di addiction.<br>     
- RIABILITAZIONE E CURA delle dipendenze da sostanze e da gioco d&rsquo;azzardo compulsivo, attraverso un centro diurno dedicato ad utenti che hanno gi&agrave; strutturato una dipendenza patologica da sostanze ed uno sportello rivolto a chi ha o sta sviluppando una ludopatia.
          </p>
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-5">
      <!--Image-->
      <img src="images/home/dipendenze.jpg" class="img-fluid z-depth-1-half" alt="">
    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</section>
<!-- Projects section v.3 -->

		
</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-6 wow fadeIn">
	<!--Call to action-->
		<div class="pt-4">
			<p>Cooperativa Sociale Azzurra 84 - Via della Balduina 61 a/b 00136 ROMA</p>
			<p>Reg. Imprese Roma nr. CF 06619940585 - P.IVA 01582561005 - C.C.I.A.A. R.E.A. 534828</br>Iscr. Albo Reg. Coop. Soc. Sez. A nr. 127/63 bis e Sez. B nr. 17/63 - Iscr. Albo Nazionale Coop.Soc. A105185</p>
		</div>

		</div>
		<!--Copyright-->
		<div class="footer-copyright py-1">
			© 2019 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Isibit</a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  </script>
</body>

</html>