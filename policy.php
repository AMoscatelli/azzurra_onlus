<?php
require_once 'php/const.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}

.policy_title{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 180%;
}

.policy_desc{
	font-family: "Kalam";
    font-weight: 100;
    font-style: normal;
    font-size: 120%;
}
</style>

</head>

<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand navbar_title" href="index.php" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA '84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item"><a class="nav-link navbar_title" href="index.php">Home<span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="servizi.php">Servizi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="attivita.php">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="successi.php">I nostri successi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="chisiamo.php">Chi siamo</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="contatti.php">Contatti</a></li>
					<li class="nav-item active"><a class="nav-link navbar_title" href="#">Privacy policy</a></li>
				
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
    					<a href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
    						class="nav-link border border-light rounded waves-effect waves-light navbar_title"
    						target="_blank"> 
    						<i class="fab fa-facebook-square mr-2"></i>Seguici su Facebook
    					</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Main layout-->
	<main>

	<div class="container" style="margin-top: 130px">

		<div class="row">
			<div class="col-md-9" data-sidebar-type='magazine-page'>
				
				<div id="mdw_team-2" class="widget-item"
					data-instance="widget_mdw_team">
					<div class="container">
						<!--Section: Team v.5-->
						<section class="section team-section">
<h1 class="policy_title">PRIVACY POLICY - GDPR </h1>
<p class="policy_desc" align="justify">Il  Regolamento  (UE)  2016/679  (anche  il  Regolamento)  e  la  relativa  normativa  italiana  di 
riferimento  (d.lgs.  196/03  cos&igrave;  come  modificato  dal d.lgs.  101/18)  stabiliscono  norme  relative 
alla protezione delle persone fisiche nell'ambito del trattamento dei dati personali e protezione 
dei diritti e delle libert&agrave; fondamentali delle persone fisiche. <br>
Nel  rispetto  della  normativa  europea  e  italiana  vigente,  AZZURRA 84  fa  in  modo  che  sia 
garantita la protezione e mantenuta la riservatezzae integrit&agrave; dei Vostri dati personali. 
La  presente  informativa  privacy  riguarda  il  trattamento  dei  dati  personali  degli  utenti  che 
navigano sul sito Internet http://www.azzurra84.it/ (di seguito, il "Sito") e su tutti i portali 
ad esso collegati, accessibili sia da pc che da dispositivi mobili; vi invitiamo a prenderne visione.</p>

<h1 class="policy_title">1.  TITOLARE DEL TRATTAMENTO </h1>

<p class="policy_desc" align="justify">Titolare del trattamento &egrave; AZZURRA 84, con sede legale in Via G. Salviucci 13 Roma, a 
cui ci si potr&agrave; rivolgere per esercitare i diritti riconosciuti dal GDPR (diritto di accesso, di rettifica, 
di  opposizione  al  trattamento  per  finalit&agrave;  commerciali  o  basato  su  processi  esclusivamente 
automatizzati, di revoca del consenso, di reclamo all'Autorit&agrave; garante per la protezione dei dati 
personali, di cancellazione, di opposizione, di limitazione e di portabilit&agrave;) o per ottenere richieste
di chiarimento in merito alla presente informativa,ai seguenti recapiti:<br> 
-  e-mail: email@azzurra84.it <br>
-  telefono: 06.35073176 <br>
-  oppure a mezzo posta ordinaria all'indirizzo sopra riportato</p>
 
<h1 class="policy_title">2.  TIPOLOGIA DI DATI - FINALITA' DEL TRATTAMENTO E BASE GIURIDICA</h1>

<p class="policy_desc" align="justify">Durante  la  navigazione  tra  le  sezioni  del  nostro  Sito,  consentita  a  qualsiasi  utente  senza 
necessit&agrave;  di  registrazione,  acquisiamo  e  trattiamo  i  seguenti  dati  personali,  per  le  specifiche 
finalit&agrave; indicate: <br>
(i)  Dati di navigazione relativi alla sessione <br>
I  sistemi  informatici  preposti  al  funzionamento  di  questo  Sito  acquisiscono,  nel  corso  del 
loro normale funzionamento, alcuni dati personali la cui trasmissione &egrave; implicita nell'uso dei 
protocolli  di  comunicazione  di  Internet  (ad  es.  indirizzi  IP,  nomi  a  dominio  dei  computer 
utilizzati dai visitatori del Sito). <br>
Fatto salvo quanto specificamente previsto in relazione all'utilizzo di  cookiessul Sito, questi 
dati vengono utilizzati al  solo fine di ricavare informazioni anonime sull'uso del Sito  e per 
controllarne il corretto funzionamento. <br>
I dati di navigazione sono trattati per garantire la sicurezza del Sito, controllarne il corretto 
funzionamento ed ottenere statistiche in relazione al suo utilizzo.<br> 
Vengono,  inoltre,  acquisiti  dati  relativi  alle  attivit&agrave;  di  navigazione,  per  sole  finalit&agrave;  statistiche 
connesse all'utilizzo del sito, attraverso l'utilizzo di cookiesdi terze parti, tra cui Google Analytics 
la  cui  informativa  &egrave;  rinvenibile  al  link  https://tools.google.com/dlpage/gaoptout?hl=it.   Non 
verranno mai rilevati e memorizzati attraverso i cookiesdati personali quali, ad esempio, nome, 
cognome, indirizzo e-mail o numero di telefono. <br>
Tali  dati  vengono  trattati  sulla  base  del  consenso  espresso  dall'utente  durante  la 
navigazione del sito web.<br> 
(ii)  Dati forniti volontariamente dall'utente <br>
Sul nostro Sito &egrave; presente un  contact form, attraverso il quale &egrave; possibile contattarci e inviarci 
richieste  di  informazioni  e/o  forniture  di  servizi. Per  tale  tipologia  di  trattamento  si  rinvia 
all'informativa specifica predisposta dal Titolare.</p>

<h1 class="policy_title">3.  MODALITA' DI TRATTAMENTO </h1>

<p class="policy_desc" align="justify">Il  trattamento  dei  dati  personali  sar&agrave;  effettuato  mediante  strumenti  informatici,  sia 
automatizzati sia non automatizzati (in tal caso, attraverso l'intervento umano nelle gestione dei 
sistemi  informatici)  e  mediante  l'utilizzo  di  strumenti  analogici  (gestione  cartacea),  secondo 
logiche strettamente correlate alle finalit&agrave; del trattamento stesse e, comunque, con il supporto 
di apparecchiature e con modalit&agrave; tali da garantirela sicurezza e la riservatezza dei dati stessi. 
Cookies. <br>
Un  "cookie"  &egrave;  una  piccola  quantit&agrave;  di  dati,  spesso  contenenti un  codice  identificativo  unico 
anonimo,  che  vengono  inviati  al  browser  dell'utente da  un  server  Web  e  che  vengono 
successivamente memorizzati sul disco fisso del computer del visitatore. <br>
Il nostro Sito utilizza c.d. cookies di sessione (che non vengono memorizzati in modo persistente 
sul computer dell'utente e svaniscono con la chiusura del browser) &egrave; strettamente limitato alla 
trasmissione  di  identificativi  di  sessione  (costituiti  da  numeri  casuali  generati  dal  server) 
necessari  per  consentire  l'esplorazione  sicura  ed  efficiente  del  sito.  I  c.d.  cookies di  sessione 
utilizzati  in  questo  Sito  evitano  il  ricorso  ad  altre  tecniche  informatiche  potenzialmente 
pregiudizievoli  per  la  riservatezza  della  navigazione  degli  utenti;  la  loro  eventuale  rimozione, 
attraverso le impostazioni del  browser, potrebbe rendere difficoltosa o addirittura impedire la 
navigazione all'interno del Sito.<br> 
Il  nostro  Sito  utilizza,  inoltre,  cookies di  terze  parti:  nello  specifico,  al  fine  di  analizzare  le 
modalit&agrave; di utilizzo del Sito, viene utilizzato Google Analytics (un servizio di analisi web fornito 
da Google Inc. che consente di raccogliere informazioni, in forma aggregata e anonima, su come 
viene utilizzato un sito internet, anche al fine dimigliorarne il funzionamento).<br> 
In occasione della prima visita al Sito, l'utente pu&ograve; accettare tutti i cookie tramite il compimento 
di una delle seguenti azioni: <br>
-  chiudendo il relativo banner, cliccando sulla X o su "Ok"; <br>
-  proseguendo la navigazione sul Sito; <br>
oppure pu&ograve; accedere alla presente informativa privacy cliccando su "No, grazie, vorrei maggiori 
informazioni"  ed  escludere  i  cookies di  Google  Analytics  seguendo  le  istruzioni  riportate  al 
seguente link: https://tools.google.com/dlpage/gaoptout?hl=it. <br>
I  cookie  possono  essere  disattivati  anche  modificando  le  impostazioni  del  proprio  browser di 
navigazione.</p> 
<h1 class="policy_title">4.  TEMPI DI CONSERVAZIONE DEI DATI </h1>

<p class="policy_desc" align="justify">I dati di navigazione vengono cancellati immediatamente dopo l'elaborazione, al termine della 
sessione di navigazione (chiusura del browser). <br>
I dati forniti volontariamente dagli utenti (attraverso la compilazione del  contact formpresente 
sul  Sito  e  l'invio  di  e-mail),  oppure  via  fax  o  per telefono,  saranno  conservati  per  i  30  giorni 
successivi all'invio agli utenti delle informazionirichieste, salvo che l'ulteriore conservazione dei
dati non sia necessaria per adempiere ad obblighi di legge e/o salvo che intervenga tra le parti.<br>
&egrave; possibile conoscere in che modo Google Inc. utilizza i  cookiese quali sono le loro caratteristiche visitando la 
pagina: https://policies.google.com/technologies/cookies?hl=it; &egrave; possibile disattivare i  cookies di Google Analytics 
alla seguente pagina: https://tools.google.com/dlpage/gaoptout?hl=it <br>
Per  la disattivazione o l'attivazione dei  cookie sono anche  disponibili  le seguenti guideper  i principali browser 
utilizzati: <br>
Chrome Internet Explorer  Firefox Safari Android iPhone, iPad  
sottoscrizione di un contratto. </p>
<h1 class="policy_title">5.  COMUNICAZIONE DEI DATI </h1>

<p class="policy_desc" align="justify">I  soggetti  che  potranno  venire  a  conoscenza  dei  dati  personali  sono  i  nostri  dipendenti, 
collaboratori, che si occupano della gestione amministrativa della pagina Web e dei rapporti con 
gli utenti. Potr&agrave;, eventualmente, venire a conoscenza dei dati personali la societ&agrave; che si occupa 
della  gestione  del  Sito,  in  occasione  di  interventi di  assistenza  /  manutenzione  ed  i  soggetti 
interessati al servizio da Voi richiesto. <br>
I dati non vengono mai trasferiti a soggetti terzi  aventi sede o comunque operanti in Paesi al di 
fuori dell'Unione Europea, all'infuori dei seguenti: Google Inc., limitatamente alle funzionalit&agrave; di 
Google Analytics, che tuttavia ha sede in un Paese (USA) che garantisce un livello di protezione 
adeguato  (essendo  incluso  all'interno  della  c.d.  Privacy  Shield  List 
https://www.privacyshield.gov/welcome). <br>
All'occorrenza,  l&agrave;  dove  avessimo  l'obbligo  di  denunciare  un  reato  o  comunque  la  necessit&agrave;  di 
perseguire un nostro legittimo interesse ad esercitare o difendere un diritto in sede giudiziale, i 
dati degli utenti potrebbero essere comunicati all'Autorit&agrave; giudiziaria o a forze di polizia. 
All'infuori  dei  casi  appena  indicati,  i  dati  personali  non  saranno  comunicati  a  terzi  al  di  fuori 
dell'ambito dell'Unione Europea e/o in alcun modo diffusi.
<h1 class="policy_title">6.  DIRITTI DELL'INTERESSATO (Art. 12-23 e 77 del Regolamento) </h1>

<p class="policy_desc" align="justify">Il Regolamento conferisce  all'interessato  l'esercizio  di specifici  diritti,  tra cui quelli di  ottenere
dal Titolare la conferma dell'esistenza o meno di propri dati personali e la loro comunicazione in 
forma intellegibile; di aver conoscenza dell'origine dei dati, delle finalit&agrave; e delle modalit&agrave; su cui 
si  basa  il  trattamento  e  della  logica  applicata  ai  casi  di  trattamento  effettuato  con  l'ausilio  di 
strumenti elettronici; di ottenere, se ne ricorranoi presupposti di legge e secondo le modalit&agrave; 
previste dal Regolamento, la cancellazione, la trasformazione in forma anonima o il blocco dei 
dati  trattati  in  violazione  di  legge,  nonch&egrave;  l'aggiornamento,  la  rettifica  o,  se  vi  sia  interesse, 
l'integrazione degli stessi; di opporsi, per motivilegittimi, al trattamento dei dati, di chiedere la
limitazione  del  trattamento  e  di  esercitare  il  diritto  alla  portabilit&agrave;.  <br>Ha  altres&igrave;  diritto,  qualora 
ritenga  che  il  trattamento  dei  propri  dati  sia  effettuato  in  violazione  delle  previsioni  del 
Regolamento,  fermo  il  diritto  di  rivolgersi  alle  competenti  autorit&agrave;  giudiziarie  civili  o 
amministrative,  di  proporre  reclamo  all'Autorit&agrave;  Garante  per  la  protezione  dei  dati  personali, 
per quanto di sua competenza. </p>

						</section>
						<!--/Section: Team v.1-->
					</div>
				</div>
			</div>
		</div>
	</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-6 wow fadeIn">
	<!--Call to action-->
		<div class="pt-4">
			<p>Cooperativa Sociale Azzurra 84 - Via della Balduina 61 a/b 00136 ROMA</p>
			<p>Reg. Imprese Roma nr. CF 06619940585 - P.IVA 01582561005 - C.C.I.A.A. R.E.A. 534828</br>Iscr. Albo Reg. Coop. Soc. Sez. A nr. 127/63 bis e Sez. B nr. 17/63 - Iscr. Albo Nazionale Coop.Soc. A105185</p>
		</div>

		</div>
		<!--Copyright-->
		<div class="footer-copyright py-1">
			© 2019 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Isibit</a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  </script>
</body>

</html>