<?php
require_once 'php/const.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}

.contatti_title{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 180%;
}

.contatti_desc{
	font-family: "Kalam";
    font-weight: 100;
    font-style: normal;
    font-size: 120%;
}

.map-container-section {
  overflow:hidden;
  padding-bottom:56.25%;
  position:relative;
  height:0;
}
.map-container-section iframe {
  left:0;
  top:0;
  height:100%;
  width:100%;
  position:absolute;
}
</style>

</head>

<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand navbar_title" href="index.php" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA '84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item"><a class="nav-link navbar_title" href="index.php">Home<span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="servizi.php">Servizi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="attivita.php">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="successi.php">I nostri successi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="chisiamo.php">Chi siamo</a></li>
					<li class="nav-item active"><a class="nav-link navbar_title" href="#">Contatti</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="policy.php">Privacy policy</a></li>
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
    					<a href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
    						class="nav-link border border-light rounded waves-effect waves-light navbar_title"
    						target="_blank"> 
    						<i class="fab fa-facebook-square mr-2"></i>Seguici su Facebook
    					</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Main layout-->
	<main>

	<div class="container" style="margin-top: 130px">
	
    	<!-- Section: Contact v.1 -->
    <section class="my-5">
    
      <!-- Section heading -->
      <h2 class="h1-responsive font-weight-bold text-center mb-4 contatti_title">AZZURRA '84 Societ&agrave; Cooperativa Sociale O.N.L.U.S.</h2>
      <!-- Section description -->
    
      <!-- Grid row -->
      <div class="row">
        
        <!-- Grid column -->
        <div class="col-lg">
    
          <!--Google map-->
          <div id="map-container-section" class="z-depth-1-half map-container-section mb-4" style="height: 400px">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2968.9059740619427!2d12.439657915803274!3d41.91637997099606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f60816c6bb087%3A0x42c4df83c36ac61e!2sCooperativa+Sociale+Azzurra+84!5e0!3m2!1sit!2sit!4v1556890000021!5m2!1sit!2sit" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <!-- Buttons-->
          <div class="row text-center">
            <div class="col-md-3">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-map-marker-alt"></i>
              </a>
              <p>Via della Balduina 61 a/b</p>
              <p class="mb-md-0"> 00136, ROMA</p>
            </div>
            <div class="col-md-3">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-phone"></i>
              </a>
              <p>06.35073176</p>
              <p class="mb-md-0">06.35072803</p>
            </div>
            <div class="col-md-3">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-fax"></i>
              </a>
              <p class="mb-md-0">06.35073152</p>
            </div>
            <div class="col-md-3">
              <a class="btn-floating blue accent-1">
                <i class="fas fa-envelope"></i>
              </a>
              <p>info@azzurra84.it</p>
              <p class="mb-0">coop.soc.azzurra84@libero.it</p>
            </div>
          </div>
    
        </div>
        <!-- Grid column -->
    
      </div>
      <!-- Grid row -->
    
    </section>
    <!-- Section: Contact v.1 -->
		
	</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-6 wow fadeIn">
	<!--Call to action-->
		<div class="pt-4">
			<p>Cooperativa Sociale Azzurra 84 - Via della Balduina 61 a/b 00136 ROMA</p>
			<p>Reg. Imprese Roma nr. CF 06619940585 - P.IVA 01582561005 - C.C.I.A.A. R.E.A. 534828</br>Iscr. Albo Reg. Coop. Soc. Sez. A nr. 127/63 bis e Sez. B nr. 17/63 - Iscr. Albo Nazionale Coop.Soc. A105185</p>
		</div>

		</div>
		<!--Copyright-->
		<div class="footer-copyright py-1">
			© 2019 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Isibit</a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  </script>
</body>

</html>