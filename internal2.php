<?php
require_once 'php/const.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}
</style>

</head>

<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand" href="index.php" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA 84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link" href="index.php">Home<span
							class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item"><a class="nav-link" href="#servizi"
						target="_self">Servizi</a></li>
					<li class="nav-item"><a class="nav-link" href="#attivta"
						target="_blank">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link" href="#successi"
						target="_blank">I nostri successi</a></li>
					<li class="nav-item"><a class="nav-link" href="#chisiamo"
						target="_blank">Chi siamo</a></li>
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<!-- <li class="nav-item">
            <a href="https://www.facebook.com/mdbootstrap" class="nav-link" target="_blank">
              <i class="fa fa-facebook"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com/MDBootstrap" class="nav-link" target="_blank">
              <i class="fa fa-twitter"></i>
            </a>
          </li> -->
					<li class="nav-item"><a
						href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
						class="nav-link border border-light rounded waves-effect waves-light"
						target="_blank"> <i class="fa fa-facebook mr-2"></i>Seguci su
							Facebook
					</a></li>
				</ul>

			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Main layout-->
	<main>

	<div class="container" style="margin-top: 130px">

		<div class="row">


			<div class='col-md-3' data-sidebar-type='magazine-sidebar'>


				<!-- Card -->
				<div class="card">

					<!-- Card image -->
					<div class="view overlay">
						<img class="card-img-top"
							src="images/internal2.jpg"
							alt="Card image cap"> <a>
							<div class="mask rgba-white-slight"></div>
						</a>
					</div>

					<!-- Button -->
					<a class="btn-floating btn-action ml-auto mr-4 mdb-color lighten-3"><i
						class="fa fa-chevron-right pl-1"></i></a>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<h4 class="card-title">Card title</h4>
						<hr>
						<!-- Text -->
						<p class="card-text">Some quick example text to build on the card
							title and make up the bulk of the card's content.</p>

					</div>

					<!-- Card footer -->
					<div class="rounded-bottom mdb-color lighten-3 text-center pt-3">
						<ul class="list-unstyled list-inline font-small">
							<li class="list-inline-item pr-2 white-text"><i
								class="fa fa-clock-o pr-1"></i>05/10/2015</li>
							<li class="list-inline-item pr-2"><a href="#" class="white-text"><i
									class="fa fa-comments-o pr-1"></i>12</a></li>
							<li class="list-inline-item pr-2"><a href="#" class="white-text"><i
									class="fa fa-facebook pr-1"> </i>21</a></li>
							<li class="list-inline-item"><a href="#" class="white-text"><i
									class="fa fa-twitter pr-1"> </i>5</a></li>
						</ul>
					</div>

				</div>
				<!-- Card -->

				<!-- Card -->
				<div class="card mt-3">

					<!-- Card image -->
					<div class="view overlay">
						<img class="card-img-top"
							src="https://mdbootstrap.com/img/Photos/Others/food.jpg"
							alt="Card image cap"> <a>
							<div class="mask rgba-white-slight"></div>
						</a>
					</div>

					<!-- Button -->
					<a class="btn-floating btn-action ml-auto mr-4 mdb-color lighten-3"><i
						class="fa fa-chevron-right pl-1"></i></a>

					<!-- Card content -->
					<div class="card-body">

						<!-- Title -->
						<h4 class="card-title">Card title</h4>
						<hr>
						<!-- Text -->
						<p class="card-text">Some quick example text to build on the card
							title and make up the bulk of the card's content.</p>

					</div>

					<!-- Card footer -->
					<div class="rounded-bottom mdb-color lighten-3 text-center pt-3">
						<ul class="list-unstyled list-inline font-small">
							<li class="list-inline-item pr-2 white-text"><i
								class="fa fa-clock-o pr-1"></i>05/10/2015</li>
							<li class="list-inline-item pr-2"><a href="#" class="white-text"><i
									class="fa fa-comments-o pr-1"></i>12</a></li>
							<li class="list-inline-item pr-2"><a href="#" class="white-text"><i
									class="fa fa-facebook pr-1"> </i>21</a></li>
							<li class="list-inline-item"><a href="#" class="white-text"><i
									class="fa fa-twitter pr-1"> </i>5</a></li>
						</ul>
					</div>

				</div>
				<!-- Card -->

			</div>
			<div class="col-md-9" data-sidebar-type='magazine-page'>
				<div id="mdw_media-2" class="widget-item"
					data-instance="widget_mdw_media">
					<div class="container mt-1" id="mdw_media-2">
						<div class=" ">
							<div class="row">
								<!--Card Light-->
								<div class="col-md-8 offset-md-2 extra-margins text-xs-center">
									<figure class="figure">
										<img
											src="images/internal_2_big.png"
											class="figure-img img-fluid text" alt="Responsive image"
											style="margin: 0 auto">

										<figcaption class="figure-caption text-xs-left"
											style="font-style: italic; padding-top: 0.3rem;"></figcaption>

									</figure>
								</div>
							</div>
						</div>
					</div>
					<!--/.Card Light-->
				</div>
				<div id="mdw_team-2" class="widget-item"
					data-instance="widget_mdw_team">
					<div class="container">
						<!--Section: Team v.5-->
						<section class="section team-section">

							<!--Section heading-->
							<h1 class="section-heading">Meet our amazing team</h1>
							<!--Section sescription-->
							<p class="section-description">Lorem ipsum dolor sit amet,
								consectetur adipisicing elit. Fugit, error amet numquam iure
								provident voluptate esse quasi, veritatis totam voluptas nostrum
								quisquam eum porro a pariatur accusamus veniam. Lorem ipsum
								dolor sit amet, consectetur adipisicing elit. Excepturi deserunt
								porro accusamus repellat autem. Ea repellat nesciunt culpa
								labore, sequi asperiores eveniet officia distinctio libero
								molestiae exercitationem.</p>
							<!--First row-->
							<div class="row">
								<div class="col-lg-3 offset-lg-1 col-md-5 mb-r " id="mdw_team-2"
									data-post-id="mdw_team-2">

									<div class="avatar">
										<img
											src="http://demo.mdwp.io/magazine/wp-content/uploads/2017/03/img-1-1.jpg"
											class="rounded-circle img-fluid">
									</div>

								</div>
								<div class="col-lg-7 text-xs-center text-sm-left mb-3">
									<h4>Anna Deynah</h4>
									<h5>Web Designer</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
										Deleniti ex cupiditate aut quis voluptatem officia rerum, quos
										at, veritatis earum eaque voluptates quia deserunt.</p>

									<a target="_blank" class="icons-sm" style="color: #000080"
										href=""><i class="fa fa-facebook"> </i></a> <a target="_blank"
										class="icons-sm" style="color: #0080ff" href=""><i
										class="fa fa-twitter"> </i></a> <a target="_blank"
										class="icons-sm" style="color: #ff0000" href=""><i
										class="fa fa-google-plus"> </i></a>
								</div>
							</div>
							<br class="hidden-sm-up">
							<div class="row">
								<div class="col-lg-3 offset-lg-1 col-md-5 mb-r " id="mdw_team-2"
									data-post-id="mdw_team-2">

									<div class="avatar">
										<img
											src="http://demo.mdwp.io/magazine/wp-content/uploads/2017/03/avatar-13.jpg"
											class="rounded-circle img-fluid">
									</div>

								</div>
								<div class="col-lg-7 text-xs-center text-sm-left mb-3">
									<h4>John Doe</h4>
									<h5>Web Developer</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
										Deleniti ex cupiditate aut quis voluptatem officia rerum, quos
										at, veritatis earum eaque voluptates quia deserunt.</p>

									<a target="_blank" class="icons-sm" style="color: #0000a0"
										href=""><i class="fa fa-facebook"> </i></a> <a target="_blank"
										class="icons-sm" style="color: #0080c0" href=""><i
										class="fa fa-twitter"> </i></a> <a target="_blank"
										class="icons-sm" style="color: #ff0000" href=""><i
										class="fa fa-google-plus"> </i></a>
								</div>
							</div>
							<br class="hidden-sm-up">



							<!--/First row-->

						</section>
						<!--/Section: Team v.1-->
					</div>
				</div>
			</div>

		</div>
	</div>


	</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-4 wow fadeIn">

		<!--Call to action-->
		<div class="pt-4">
			<a class="btn btn-outline-white"
				href="https://mdbootstrap.com/getting-started/" target="_blank"
				role="button">Lorem ipsum dolor sit amet <i
				class="fa fa-download ml-2"></i>
			</a> <a class="btn btn-outline-white"
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank"
				role="button">Lorem ipsum dolor sit amet <i
				class="fa fa-graduation-cap ml-2"></i>
			</a>
		</div>
		<!--/.Call to action-->

		<hr class="my-4">

		<!-- Social icons -->
		<div class="pb-4">
			<a href="https://www.facebook.com/mdbootstrap" target="_blank"> <i
				class="fa fa-facebook mr-3"></i>
			</a> <a href="https://twitter.com/MDBootstrap" target="_blank"> <i
				class="fa fa-twitter mr-3"></i>
			</a> <a href="https://www.youtube.com/watch?v=7MUISDJ5ZZ4"
				target="_blank"> <i class="fa fa-youtube mr-3"></i>
			</a> <a href="https://plus.google.com/u/0/b/107863090883699620484"
				target="_blank"> <i class="fa fa-google-plus mr-3"></i>
			</a> <a href="https://dribbble.com/mdbootstrap" target="_blank"> <i
				class="fa fa-dribbble mr-3"></i>
			</a> <a href="https://pinterest.com/mdbootstrap" target="_blank"> <i
				class="fa fa-pinterest mr-3"></i>
			</a> <a
				href="https://github.com/mdbootstrap/bootstrap-material-design"
				target="_blank"> <i class="fa fa-github mr-3"></i>
			</a> <a href="http://codepen.io/mdbootstrap/" target="_blank"> <i
				class="fa fa-codepen mr-3"></i>
			</a>
		</div>
		<!-- Social icons -->

		<!--Copyright-->
		<div class="footer-copyright py-3">
			© 2018 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Lorem ipsum dolor sit amet </a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  </script>
</body>

</html>