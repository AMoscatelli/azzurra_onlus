<?php
require_once 'php/const.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}

.attivita_title{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 260%;
}

.attivita_desc{
	font-family: "Kalam";
    font-weight: 100;
    font-style: normal;
    font-size: 120%;
}

.attivita_desc2{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 160%;
}

.font_custom{
	font-family: "Kalam";
}


</style>

</head>

<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand navbar_title" href="index.php" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA '84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item"><a class="nav-link navbar_title" href="index.php">Home<span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="servizi.php">Servizi</a></li>
					<li class="nav-item active"><a class="nav-link navbar_title" href="#">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="successi.php">I nostri successi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="chisiamo.php">Chi siamo</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="contatti.php">Contatti</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="policy.php">Privacy policy</a></li>
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
    					<a href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
    						class="nav-link border border-light rounded waves-effect waves-light navbar_title"
    						target="_blank"> 
    						<i class="fab fa-facebook-square mr-2"></i>Seguici su Facebook
    					</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Main layout-->
	<main>
<div class="container" style="margin-top: 130px">

<!-- Section: Team v.3 -->
<section class="team-section">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center font_custom">Le nostre attivit&agrave;</h2>
  <!-- Section description -->
  <p class="text-center mx-auto mt-4 mb-5 attivita_desc2">Servizi alle persone <br>(minori, famiglie, persone in situazioni di fragilit&agrave;, dipendenze)</p>

  <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/sostegno.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Servizi a sostegno dell&rsquo;autonomia e dell&rsquo;integrazione dei bambini ed adolescenti disabili nelle scuole dell&rsquo;infanzia comunali e statali, elementari e medie inferiori</h4>
        <p class="grey-text font_custom" align="justify">Si tratta di interventi, attivati su richiesta del Municipio, che mirano a fornire aiuto per promuovere e potenziare l&rsquo;autonomia e l&rsquo;integrazione sociale dei bambini e adolescenti disabili frequentanti le sedi scolastiche site nel territorio di diversi Municipi del territorio comunale. Gli interventi sono realizzati ad personale formato (AEC).</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->

  <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/teleassistenza.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Servizio Municipale di Teleassistenza</h4>
        <p class="grey-text font_custom" align="justify">Rivolto ad anziani e disabili del territorio del Municipio 14. Il servizio offre interventi di assistenza leggera, attraverso commissioni ed accompagni, per aiutare le persone a far fronte alle esigenze quotidiane e favorire la permanenza nel proprio domicilio ricevendo aiuto nello svolgimento di azioni pi&ugrave; complesse o che richiedono una mobilit&agrave; che pu&ograve; essere per la persona limitata.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/attivamente.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Attivamente</h4>
        <p class="grey-text font_custom" align="justify">Progetto finanziato dalla Regione Lazio, rivolto a giovani tra i 18 e i 29 anni in condizioni di disagio economico e sociale (giovani inoccupati, famiglie multiproblematiche, persone a rischio per uso stupefacenti e micro criminalit&agrave;) con l&rsquo;obiettivo di migliorare l&rsquo;occupabilit&agrave;, favorire la riduzione della povert&agrave; e dell&rsquo;esclusione sociale. Ci&ograve; attraverso azioni di assessment, orientamento, sostegno psicologico, sostegno legale, azioni di empowerment individuale e di gruppo, tutoraggio per l&rsquo;accesso ad opportunit&agrave; formative e d&rsquo;inserimento lavorativo e sociale.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/famiglie.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Centro per le famiglie</h4>
        <p class="grey-text font_custom" align="justify">Progetto realizzato nel territorio del Municipio 14 in A.T.I. con S.Onofrio Coop. Sociale Onlus. Si rivolge alle famiglie ed ai minori del territorio realizzando interventi integrati di tipo psicologico, sociale, educativo e legale finalizzati al supporto e alla facilitazione dei percorsi evolutivi.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>

  
      <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/casafamiglie.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Una casa per le famiglie</h4>
        <p class="grey-text font_custom" align="justify">Il servizio, realizzato nel territorio del Municipio IX, opera per garantire il  diritto di &ldquo;buona crescita&rdquo; e sostenere la necessit&agrave; di crescere all&rsquo;interno di relazioni funzionali, di essere protetti e preservati e/o curati a seguito di violazioni fisiche ed emozionali, volendo dunque svolgere un&rsquo;efficace azione di promozione della salute, secondo la definizione dell&rsquo;OMS. Attraverso interventi psicoeducativi, di consulenza e sostegno, mediazione familiare e spazio di incontri, si fornisce un supporto concreto alle famiglie nella gestione dei passaggi critici e nell&rsquo;affrontare problematiche ed interruzioni relazionali.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/genitori.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Essere Genitori... che responsabilit&agrave;</h4>
        <p class="grey-text font_custom" align="justify">Si tratta di un servizio di sostegno alla genitorialit&agrave;, prevenzione e contrasto del disagio in ambito familiare e minorile, con particolare riferimento alla fascia adolescenziale (anni 11-18), attraverso interventi di consulenza e informazione realizzati da assistenti sociali nel territorio del Municipio IX.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
      <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/assistenzadomiciliare.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Servizi di cura domiciliare</h4>
        <p class="grey-text font_custom" align="justify">Attivi  nel territori dei Municipi.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
  <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/sert.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Centro specialistico semiresidenziale per trattamento psicopatologia</h4>
        <p class="grey-text font_custom" align="justify">In partenariato con la ASL ROMA 1. Il Centro, opera per prevenire il rischio di devianza ed emarginazione sociale per gli utenti del Ser.T.tossicodipendenti con sottostanti disturbi di tipo psichiatrico attraverso un contesto protetto. All&rsquo;interno del Centro Diurno si svolgono attivit&agrave; terapeutiche, riabilitative e formative per abbassare le probabilit&agrave; di ricaduta, mantenere lo stato di astinenza, ridurre i comportamenti devianti, recuperare risorse familiari e personali per prevenire i processi di autoemarginazione e migliorare le capacit&agrave; di far fronte agli eventi di vita e alle tematiche di dipendenza.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/azzardo.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Servizio di osservazione diagnostica e presa in carico del paziente con DGA (disturbo da gioco d�azzardo)</h4>
        <p class="grey-text font_custom" align="justify">Il servizio, realizzato in convenzione con la ASL ROMA 1, mira alla definizione di programmi terapeutici personalizzati e all&rsquo;invio di pazienti con DGA e pazienti tossicodipendenti presso strutture riabilitative idonee.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row- -->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/giustizia.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Interventi sociali a favore di nuclei familiari con minori anche sottoposti a provvedimento di magistratura</h4>
        <p class="grey-text font_custom" align="justify">Il progetto realizza l&rsquo;attivit&agrave; di un servizio sociale professionale nell&rsquo;area della giustizia minorile, in convenzione con il Municipio Roma XIII presso la sede del Servizio Sociale.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row- -->
  
   <!-- Section description -->
  <p class="text-center mx-auto mt-4 mb-5 attivita_desc2">Servizi di Informazione, orientamento e facilitazione di accesso ai servizi del territorio <br>(realizzati in convenzione con i Municipi del Comune di Roma)</p>

  <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/munV.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Municipio V: Punto Unico di Accesso integrato socio sanitario (PUA)</h4>
        <p class="grey-text font_custom" align="justify">Il servizio rappresenta una porta d&rsquo;accesso ai servizi socio-sanitari e fornisce un&rsquo;accoglienza professionale al cittadino per avviare percorsi di presa in carico ed orientare per aiutare le persone ad orientarsi e fruire dei servizi disponibili nel territorio.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/munXI.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Municipio XI: Punto Unico di Accesso socio sanitario e segretariato sociale</h4>
        <p class="grey-text font_custom" align="justify">Il servizio fornisce risposte sui diversi servizi esistenti nel territorio, sulle procedure per accedervi, le prestazioni, le normative utili ad effettuare una libera scelta tra le risorse sociali disponibili e a poterle utilizzare correttamente.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
  <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/munIV.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Municipio IV: Implementazione del Segretariato Sociale Punto Unico di Accesso</h4>
        <p class="grey-text font_custom" align="justify">Il servizio vuole creare un sistema di accoglienza, informazione e orientamento per le richieste dei cittadini e garantire una facilit&agrave; di accesso ai servizi sociali, sanitari, socio-sanitari del territorio.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/munXV.png" class="rounded z-depth-1" alt="Sample avatar">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 attivita_desc">Municipio XV: Punto Unico di Accesso</h4>
        <p class="grey-text font_custom" align="justify">Il servizio intende fornire ai cittadini informazioni sulle risorse presenti nel territorio e sulle modalit&agrave; per accedervi: in questa prospettiva opera una ricezione della richiesta e un orientamento/invio alle strutture idonee a soddisfarla, ponendosi come cerniera tra i cittadini e i servizi territoriali. Presso lo sportello &eacute; inoltre attivo uno specifico servizio di informazione e orientamento al lavoro.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
  </div>

</section>
<!-- Section: Team v.3 -->

		
</div>
</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-6 wow fadeIn">
	<!--Call to action-->
		<div class="pt-4">
			<p>Cooperativa Sociale Azzurra 84 - Via della Balduina 61 a/b 00136 ROMA</p>
			<p>Reg. Imprese Roma nr. CF 06619940585 - P.IVA 01582561005 - C.C.I.A.A. R.E.A. 534828</br>Iscr. Albo Reg. Coop. Soc. Sez. A nr. 127/63 bis e Sez. B nr. 17/63 - Iscr. Albo Nazionale Coop.Soc. A105185</p>
		</div>

		</div>
		<!--Copyright-->
		<div class="footer-copyright py-1">
			© 2019 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Isibit</a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  </script>
</body>

</html>