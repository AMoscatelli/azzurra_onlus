<?php
require_once 'php/const.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}

.chisiamo_title{
	font-family: "Kalam";
    font-weight: 400;
    font-style: normal;
    font-size: 260%;
}

.chisiamo_desc{
	font-family: "Kalam";
    font-weight: 100;
    font-style: normal;
    font-size: 120%;
}

.font_custom{
	font-family: "Kalam";
}


</style>

</head>

<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand navbar_title" href="index.php" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA '84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item"><a class="nav-link navbar_title" href="index.php">Home<span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="servizi.php">Servizi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="attivita.php">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="successi.php">I nostri successi</a></li>
					<li class="nav-item active"><a class="nav-link navbar_title" href="#">Chi siamo</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="contatti.php">Contatti</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="policy.php">Privacy policy</a></li>
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
    					<a href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
    						class="nav-link border border-light rounded waves-effect waves-light navbar_title"
    						target="_blank"> 
    						<i class="fab fa-facebook-square mr-2"></i>Seguici su Facebook
    					</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

	<!--Main layout-->
	<main>
<div class="container" style="margin-top: 130px">
	
    	<!-- Section: Contact v.1 -->
<section>
    
       <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center mb-3 chisiamo_title">CHI SIAMO</h2>
  <!-- Section description -->
  <p class="mb-5 chisiamo_desc" align="justify">
	Inizialmente nata come struttura che vuole offrire un sostegno concreto ai ragazzi disabili, nel 1996 Azzurra '84 ha esteso il suo raggio d&rsquo;azione, volendo porre in essere una serie di azioni capaci di dare sostegno alla comunit&aacute; in generale. Oggi, in qualit&aacute; di cooperativa sociale, ci rivolgiamo ad anziani, minori e loro famiglie, persone in difficolt&aacute; (detenuti, ex detenuti, ex tossicodipendenti, immigrati, giovani a rischio, ecc), portatori di disagi fisici e psichici, oltre che a persone che affrontino momenti critici del loro percorso di vita, realizzando interventi e servizi di prevenzione, cura, assistenza, riabilitazione, anche attraverso l&rsquo;inserimento al lavoro di persone in condizioni di svantaggio. <br>
	In pi&ugrave; di venti anni di esperienza abbiamo realizzato innumerevoli progetti nel territorio della Regione Lazio operando sia nel campo della normalit&aacute; che della marginalit&aacute;, permettendo l&rsquo;inserimento lavorativo di un centinaio di persone con disabilit&aacute; o storie di marginalit&aacute;.
	<br>
	<br>
	Lo staff:
  </p>
  
      <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="col-12 mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/legale.png" class="rounded z-depth-1" alt="Castrataro">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 font_custom">Dr. Andrea Castrataro</h4>
        <h6 class="font-weight-bold grey-text mb-3 font_custom">Rappresentante Legale</h6>
        <p class="font_custom">Sono il legale rappresentate della cooperativa e mi occupo di molte cose, dalle parti burocratiche e di responsaiblit&agrave; che competono ad un rappresentante legale fino ad azioni pi&ugrave; particolari, garantendo anche il sostegno, l&rsquo;aiuto  e la sostituzione degli operatori quando ce ne sia bisogno. </p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
      <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="col-12 mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/carlesimo.png" class="rounded z-depth-1" alt="Castrataro">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 font_custom">Dott.ssa Silvana Carlesimo</h4>
        <h6 class="font-weight-bold grey-text mb-3 font_custom">Psicologa</h6>
        <p class="font_custom">Sono psicologa ad  orientamento psicodinamico, esperta in psicodiagnosi e valutazione della psicopatologia dell&rsquo;adulto in ambito clinico, peritale, di ricerca e di formazione.<br>
			Mi occupo  di progettazione di interventi psicosociali e del  coordinamento di progetti rivolti alla prevenzione delle dipendenze patologiche e alla gestione del disagio psichico.
		</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
  <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="col-12 mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/lorefice.png" class="rounded z-depth-1" alt="Castrataro">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 font_custom">Dott.ssa Loredana Lorefice</h4>
        <h6 class="font-weight-bold grey-text mb-3 font_custom">Pedagoga</h6>
        <p class="font_custom">Sono pedagogista, mediatrice familiare e responsabile della progettazione e coordinatore di servizi complessi  e di sostenere le attivit&agrave; di rete nel territorio.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
    <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="col-12 mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/serfilippi.png" class="rounded z-depth-1" alt="Castrataro">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 font_custom">Tania Serfilippi</h4>
        <h6 class="font-weight-bold grey-text mb-3 font_custom">Amministrazione</h6>
        <p class="font_custom">Mi occupo di amministrazione e contabilit&agrave; con precisione ed attenzione, lavorando presso la cooperativa da oltre un ventennio ed essendo una delle memorie storiche dei molti progetti e dei molti professionisti ed operatori che ci hanno arricchito nel corso del tempo.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
  
      <!-- Grid row-->
  <div class="row text-center text-md-left">
    <!-- Grid column -->
    <div class="col-12 mb-5 d-md-flex justify-content-between">
      <div class="avatar mb-md-0 mb-4 mx-4">
        <img src="images/alessandra.png" class="rounded z-depth-1" alt="Castrataro">
      </div>
      <div class="mx-4">
        <h4 class="font-weight-bold mb-3 font_custom">Alessandra Giordano</h4>
        <h6 class="font-weight-bold grey-text mb-3 font_custom">Segreteria</h6>
        <p class="font_custom">Mi occupo della segreteria, della gestione amministrativa e contabile. Lavoro dal 2000 in Cooperativa e sono un punto di riferimento per le molte richieste che arrivano.</p>
      </div>
    </div>
    <!-- Grid column -->
  </div>
  <!-- Grid row-->
    
</section>
    <!-- Section: Contact v.1 -->
		
	</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-6 wow fadeIn">
	<!--Call to action-->
		<div class="pt-4">
			<p>Cooperativa Sociale Azzurra 84 - Via della Balduina 61 a/b 00136 ROMA</p>
			<p>Reg. Imprese Roma nr. CF 06619940585 - P.IVA 01582561005 - C.C.I.A.A. R.E.A. 534828</br>Iscr. Albo Reg. Coop. Soc. Sez. A nr. 127/63 bis e Sez. B nr. 17/63 - Iscr. Albo Nazionale Coop.Soc. A105185</p>
		</div>

		</div>
		<!--Copyright-->
		<div class="footer-copyright py-1">
			© 2019 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Isibit</a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  </script>
</body>

</html>