<?php
require_once 'php/const.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Coop. Azzurra 84</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Font Google -->
<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
<!-- Animate CSS -->
<link rel="stylesheet" href="css/animate.css">
<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="css/mdb.min.css" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="cssMW/style.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="shortcut icon" type="image/ico"
	href="<?php echo ROOT_ ?>favicon.ico" />
<style type="text/css">
/* Necessary for full page carousel*/
html, body, header, .view {
	height: 100%;
}

/* Carousel*/
.carousel, .carousel-item, .carousel-item.active {
	height: 100%;
}

.carousel-inner {
	height: 100%;
}

@media ( min-width : 800px) and (max-width: 850px) {
	.navbar:not (.top-nav-collapse ) {
		background: #1C2331 !important;
	}
}
</style>

</head>


<body>

	<!-- Navbar -->
	<nav
		class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
		<div class="container">

			<!-- Brand -->
			<a class="navbar-brand navbar_title" href="#" target="_blank">
				<strong> <img class="rounded" src="<?php echo IMAGES_?>logo.png"
					alt="Smiley face"> AZZURRA '84
			</strong>
			</a>

			<!-- Collapse -->
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<!-- Links -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Left -->
				<ul id="top-menu" class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link navbar_title" href="#carousel-example-1z">Home<span class="sr-only">(current)</span></a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="#servizi">Servizi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="#attivta">Attivit&agrave;</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="#successi">I nostri successi</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="#chisiamo">Chi siamo</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="contatti.php">Contatti</a></li>
					<li class="nav-item"><a class="nav-link navbar_title" href="policy.php">Privacy policy</a></li>
					
				</ul>

				<!-- Right -->
				<ul class="navbar-nav nav-flex-icons">
					<li class="nav-item">
    					<a href="https://www.facebook.com/CooperativaSocialeAzzurra84o.n.l.u.s/"
    						class="nav-link border border-light rounded waves-effect waves-light navbar_title"
    						target="_blank"> 
    						<i class="fab fa-facebook-square mr-2"></i>Seguici su Facebook
    					</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
	<!-- Navbar -->

        <!-- Slideshow -->
        <!--Carousel Wrapper-->
        <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
          <!--Indicators-->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-2" data-slide-to="1"></li>
            <li data-target="#carousel-example-2" data-slide-to="2"></li>
            <li data-target="#carousel-example-2" data-slide-to="3"></li>
            <li data-target="#carousel-example-2" data-slide-to="4"></li>
            <li data-target="#carousel-example-2" data-slide-to="5"></li>
          </ol>
          <!--/.Indicators-->
          <!--Slides-->
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <div class="view">
                <img class="d-block w-100" src="images/slideshow/fb2.jpg" alt="First slide">
                <div class="mask rgba-black-strong"></div>
              </div>
              <div class="carousel-caption" id="scritta_azzurra">
<!--                 <h3 class="h3-responsive wow fadeInUp slideshow text-right">AZZURRA '84</h3> -->
                <h3 class="h3-responsive wow fadeInLeft slideshow2 text-right" data-wow-delay="0.5s">Se mi confondo.. guidami e sii paziente..</h3>
                <h3 class="h3-responsive wow fadeInRight slideshow2 text-right" data-wow-delay="1s">- Gomez de Teran -</h3>
                
<!--                 <p>First text</p> -->
              </div>
            </div>
            <div class="carousel-item">
              <!--Mask color-->
              <div class="view">
                <img class="d-block w-100" src="images/slideshow/fb3.jpg" alt="Second slide">
                <div class="mask rgba-black-slight"></div>
              </div>
              <div class="carousel-caption">
<!--                 <h3 class="h3-responsive">Strong mask</h3> -->
<!--                 <p>Secondary text</p> -->
              </div>
            </div>
            <div class="carousel-item">
              <!--Mask color-->
              <div class="view">
                <img class="d-block w-100" src="images/slideshow/fb1.jpg" alt="Third slide">
                <div class="mask rgba-black-slight"></div>
              </div>
              <div class="carousel-caption">
<!--                 <h3 class="h3-responsive">Slight mask</h3> -->
<!--                 <p>Third text</p> -->
              </div>
            </div>
            <div class="carousel-item">
              <!--Mask color-->
              <div class="view">
                <img class="d-block w-100" src="images/slideshow/fb4.jpg" alt="Third slide">
                <div class="mask rgba-black-slight"></div>
              </div>
              <div class="carousel-caption">
<!--                 <h3 class="h3-responsive">Slight mask</h3> -->
<!--                 <p>Third text</p> -->
              </div>
            </div>
            <div class="carousel-item">
              <!--Mask color-->
              <div class="view">
                <img class="d-block w-100" src="images/slideshow/fb5.jpg" alt="Third slide">
                <div class="mask rgba-black-slight"></div>
              </div>
              <div class="carousel-caption">
<!--                 <h3 class="h3-responsive">Slight mask</h3> -->
<!--                 <p>Third text</p> -->
              </div>
            </div>
            <div class="carousel-item">
              <!--Mask color-->
              <div class="view">
                <img class="d-block w-100" src="images/slideshow/fb6.png" alt="Third slide">
                <div class="mask rgba-black-slight"></div>
              </div>
              <div class="carousel-caption">
<!--                 <h3 class="h3-responsive">Slight mask</h3> -->
<!--                 <p>Third text</p> -->
              </div>
            </div>
          </div>
          <!--/.Slides-->
          <!--Controls-->
          <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          <!--/.Controls-->
        </div>
        <!--/.Carousel Wrapper-->
        <!-------->

	<!--Main layout-->
	<main>
	<div class="container">

		<!--Section: SERVIZIO-->
		<section id="servizi" class="mt-5">
			<h3 class="h3 text-center mb-5" id="servizi_title">SERVIZI</h3>
			<!--Grid row-->
			<div class="row">

				<!--Immagine prima colonna-->
				<div class="col-md-3 mb-4 wow rubberBand"> 
				<!-- data-wow-duration="2s" data-wow-delay="5s" -->
					<img src="images/home/family.jpg" class="img-fluid z-depth-1-half" alt="">   
				</div>
				<!--------->

				<!--Immagine seconda colonna-->
				<div class="col-md-3 mb-4 wow rubberBand" data-wow-delay="0.5s">

					<img src="images/home/inserimento.jpg" class="img-fluid z-depth-1-half" alt="">

				</div>
				<!--------->

				<!--Immagine terza colonna-->
				<div class="col-md-3 mb-4 wow rubberBand" data-wow-delay="1s">

					<img src="images/home/disabilita.jpg" class="img-fluid z-depth-1-half" alt="">

				</div>
				<!--------->
				
				<!--Immagine quarta colonna-->
				<div class="col-md-3 mb-4 wow rubberBand" data-wow-delay="1.5s">

					<img src="images/home/dipendenze.jpg" class="img-fluid z-depth-1-half" alt="">

				</div>
				<!--------->


				<!--Descrizione immagine prima colonna-->
				<div class="col-md-3 mb-4">

					<!-- Main heading -->
					<h4 class="font-weight-bold text-center mb-3 service" id="service">Sostegno a minori e famiglie</h4>
					<p class="text-center"><a href="servizi.php"class="font-weight-bold service2 scopri">Scopri di piu...</a>
					</p>

				</div>
				<!--------->


				<!--Descrizione immagine seconda colonna-->
				<div class="col-md-3 mb-4">

					<!-- Main heading -->
					<h4 class="font-weight-bold text-center mb-3 service" id="service">Inserimento socio-lavorativo</h4>
					<p class="text-center"><a href="servizi.php"class="font-weight-bold service2 scopri">Scopri di piu...</a>
					</p>

				</div>
				<!--------->


				<!--Descrizione immagine terza colonna-->
				<div class="col-md-3 mb-4">

					<!-- Main heading -->
					<h4 class="font-weight-bold text-center mb-3 service" id="service">Sostegno all&rsquo;autonomia di persone con fragilit&aacute;</h4>
					<p class="text-center"><a href="servizi.php"class="font-weight-bold service2 scopri">Scopri di piu...</a>
					</p>

				</div>
				<!--------->
				
				<!--Descrizione immagine quarta colonna-->
				<div class="col-md-3 mb-4">

					<!-- Main heading -->
					<h4 class="font-weight-bold text-center mb-3 service" id="service">Interventi nelle aree delle dipendenze</h4>
					<p class="text-center"><a href="servizi.php"class="font-weight-bold service2 scopri">Scopri di piu...</a>
					</p>

				</div>
				<!--------->

			</div>
			<!--------->

		</section>

		<hr class="mt-2">

		<!--Section: Main features & Quick Start-->
		<section id="attivta">

			<h3 class="h3 text-center mb-4" id="servizi_title">ATTIVITA'</h3>

			<p class="black-text mt-3 mb-3 wow fadeInUp" id="attibit�_desc">L&rsquo;obiettivo generale della Cooperativa Azzurra &lsquo;84 &egrave; quello di essere interlocutore 
			privilegiato delle famiglie e degli enti interessati al mondo della disabilit&agrave; e del disagio 
			sociale, assicurando la soddisfazione di esigenze ed aspettative di utenti, committenti e della comunit&agrave; territoriale.
			La Cooperativa promuove una serie di interventi volti a favorire il raggiungimento dell&rsquo;oggetto 
			sociale nel rispetto di alcuni principi fondanti quali: </p>
			
            <!-- Grid row -->
            <div class="row mt-5">
            
              <div class="col-lg-1"></div>
              <!-- Grid column -->
              <div class="col-lg-10 ">
            
                <div class="media d-block d-md-flex">
                  <img class="d-flex rounded-circle avatar z-depth-1-half mb-3 mx-auto wow bounceInLeft" src="images/sostegno.png" alt="Avatar">
                  <div class="media-body text-center text-md-left ml-md-3 ml-0">
                    <h5 class="mt-0 font-weight-bold blue-text attivita">Servizi a sostegno dell&rsquo;autonomia e dell&rsquo;integrazione</h5>
            			<p class="desc_attivita">Servizi a sostegno dell&rsquo;autonomia e dell&rsquo;integrazione dei bambini ed adolescenti disabili nelle scuole dell&rsquo;infanzia comunali e statali, elementari e... <a href="attivita.php"
            									class="font-weight-bold text-custom3">continua a leggere...</a></p>
            
                    <div class="media d-block d-md-flex mt-5 shadow-textarea">
                      <img class="d-flex rounded-circle avatar z-depth-1-half mb-3 mx-auto wow bounceInLeft" data-wow-delay="0.5s" src="images/teleassistenza.png" alt="Generic placeholder image">
                      <div class="media-body text-center text-md-left ml-md-3 ml-0">
                        <h5 class="mt-0 font-weight-bold blue-text mb-3 attivita">Servizio Municipale di Teleassistenza</h5>
            				<p class="desc_attivita">Servizi a sostegno dell&rsquo;autonomia e dell&rsquo;integrazione dei bambini ed adolescenti disabili nelle scuole dell&rsquo;infanzia comunali e statali, elementari e... <a href="attivita.php"
            									class="font-weight-bold text-custom3">continua a leggere...</a></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="media d-block d-md-flex mt-3">
                  <img class="d-flex rounded-circle avatar z-depth-1-half mb-3 mx-auto wow bounceInLeft"  data-wow-delay="1s"src="images/attivamente.png"
                    alt="Avatar">
                  <div class="media-body text-center text-md-left ml-md-3 ml-0">
                    <h5 class="mt-0 font-weight-bold blue-text attivita">Attivamente</h5>
            			<p class="desc_attivita">Progetto finanziato dalla Regione Lazio,  rivolto a giovani tra  i  18  e  i  29  anni  in  condizioni  di  disagio  economico  e  sociale  (giovani  inoccupati, famiglie multiproblematiche, persone a rischio per uso stupefacenti...
            			<a href="attivita.php" class="font-weight-bold text-custom3">continua a leggere...</a></p>
            		 <div class="media d-block d-md-flex mt-5 shadow-textarea">
                      <img class="d-flex rounded-circle avatar z-depth-1-half mb-3 mx-auto wow bounceInLeft" data-wow-delay="1.5s" src="images/pua.png" alt="Generic placeholder image">
                      <div class="media-body text-center text-md-left ml-md-3 ml-0">
                        <h5 class="mt-0 font-weight-bold blue-text mb-3 attivita">Punto Unico di Accesso integrato socio sanitario (PUA)</h5>
            				<p class="desc_attivita">Il servizio rappresenta una porta d&rsquo;accesso ai servizi socio-sanitari e fornisce un&rsquo;accoglienza professionale al cittadino per avviare percorsi di presa in carico ed orientare... 
            				<a href="attivita.php" class="font-weight-bold text-custom3">continua a leggere...</a></p>
                      </div>
                    </div>
                  </div>
                </div>
            
              </div>
              <div class="col-lg-1"></div>
              <!-- Grid column -->
            
            </div>
            <!-- Grid row -->

		</section>
		<!--Section: Main features & Quick Start-->

		<hr>
		
		<!-- Projects section v.3 -->
<section class="mb-3" id="successi">

  <!-- Section heading -->
<!--   <h2 class="h1-responsive font-weight-bold text-center my-5">Our best projects</h2> -->
  <h2 class="text-center" id="chi_siamo">I NOSTRI SUCCESSI</h2>

  <!-- Grid row -->
  <div class="row mt-5">

    <!-- Grid column -->
    <div class="col-lg-5 mb-lg-0 mb-5">
      <!--Image-->
      <img src="images/success1.png" alt="Sample project image" class="img-fluid rounded z-depth-1 wow fadeIn" >
    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
<!--           <i class="fas fa-book fa-2x cyan-text"></i> -->
        </div>
        <div class="col-md-11 col-10">
          <h5 class="font-weight-bold mb-3 success">CITY CARE - SPORTELLO SOCIALE</h5>
          <p class="success_desc">Realizzazione in ATI con Associazione Crescere Insieme onlus, Associazione La promessa onlus, Istituto di Psicologia del S&eacute; e Psicoanalisi <a href="successi.php" class="font-weight-bold success_desc">continua a leggere...</a></p>
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
<!--           <i class="fas fa-code fa-2x red-text"></i> -->
        </div>
        <div class="col-md-11 col-10">
          <h5 class="font-weight-bold mb-3 success">VISITE GUIDATE PROGETTO ZETEMA</h5>
          <p class="success_desc">Realizzazione di un servizio di organizzazione e accompagnamento gruppi cittadini disabili &quot;Visite guidate progetto Zetema&quot; in convenzione con il Comune di Roma <a href="successi.php" class="font-weight-bold success_desc">continua a leggere...</a></p>
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row" id="chisiamo">
        <div class="col-md-1 col-2">
<!--           <i class="far fa-money-bill-alt fa-2x deep-purple-text"></i> -->
        </div>
        <div class="col-md-11 col-10">
          <h5 class="font-weight-bold mb-3 success">LUDOTECA E LUDOBUS</h5>
          <p class="success_desc mb-0">Realizzazione di un progetto denominato &quot;Ludoteca e Ludobus&quot;  D.D. 2861 del 27/12/06, in convenzione con il Municipio Roma 18 <a href="successi.php" class="font-weight-bold success_desc">continua a leggere...</a></p>
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

  <hr class="my-5">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-7">

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
<!--           <i class="far fa-chart-bar fa-2x indigo-text"></i> -->
        </div>
        <div class="col-md-11 col-10">
          <h5 class="font-weight-bold mb-3 success">UNA CASA PER LE FAMIGLIE</h5>
          <p class="success_desc">Realizzazione del progetto L.285/97 denominato &quot;Una casa per le famiglie&quot; in convenzione con il Municipio Roma IX (ex Roma XII) <a href="successi.php" class="font-weight-bold success_desc">continua a leggere...</a></p>
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-3">
        <div class="col-md-1 col-2">
<!--           <i class="fas fa-music fa-2x pink-text"></i> -->
        </div>
        <div class="col-md-11 col-10">
          <h5 class="font-weight-bold mb-3 success">SERVIZI DI CURA DOMICILIARE</h5>
          <p class="success_desc">Idoneit&agrave; in proroga per la realizzazione di &quot;Servizi di cura domiciliare&quot; Area Anziani, Area Disabili, Area Minori presso il Municipio Roma I <a href="successi.php" class="font-weight-bold success_desc">continua a leggere...</a></p>
        </div>
      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row mb-lg-0 mb-5">
        <div class="col-md-1 col-2">
<!--           <i class="far fa-grin fa-2x blue-text"></i> -->
        </div>
        <div class="col-md-11 col-10">
          <h5 class="font-weight-bold mb-3 success">ORIENTAMENTO E INSERIMENTO LAVORATIVO</h5>
          <p class="success_desc mb-0">Realizzazione in ATI con Associazione La promessa onlus del progetto  &quot;Orientamento e inserimento lavorativo&quot; azione n.3 in convenzione con l&rsquo;Agenzia Capitolina <a href="successi.php" class="font-weight-bold success_desc">continua a leggere...</a></p>
        </div>
      </div>
      <!-- Grid row -->

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-5">
      <!--Image-->
      <img src="images/success2.png" alt="Sample project image" class="img-fluid rounded z-depth-1 wow fadeIn" data-wow-delay="0.5s">
    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</section>
<!-- Projects section v.3 -->

		<hr>

		<!--Section: More-->
		<section id="">

			<h2 class="text-center" id="chi_siamo">CHI SIAMO</h2>
				<p class="black-text wow fadeInUp mt-3 text-center" id="chisiamo_desc">Una cooperativa sociale, senza finalit&agrave; di lucro, che si ispira ai principi della mutualit&agrave; 
				e della solidariet&agrave; sociale proponendosi di perseguire l'interesse generale della comunit&agrave; attraverso la promozione
				 e l'integrazione sociale delle persone <a href="chisiamo.php" class="font-weight-bold text-custom3">continua a leggere...</a></p>
				 
			<!--Row immagini-->
			<div class="row features-small mt-5 mb-4">
				<div class="col-1"> 
				</div>
				<!--Grid column-->
				<div class="col-2 text-center wow bounceIn" data-wow-delay="1s">
					<img src="images/alessandra.png" class="z-depth-0 img-fluid">
				</div>
				<!--/Grid column-->

				<!--Grid column-->
				<div class="col-2 text-center wow bounceIn" data-wow-delay="1.5s">
					<img src="images/legale.png" class="z-depth-0 img-fluid">
				</div>
				<!--/Grid column-->

				<!--Grid column-->
				<div class="col-2 text-center wow bounceIn" data-wow-delay="2s">
					<img src="images/lorefice.png" class="z-depth-0 img-fluid">
				</div>
				<!--/Grid column-->
				
				<!--Grid column-->
				<div class="col-2 text-center wow bounceIn" data-wow-delay="2.5s">
					<img src="images/serfilippi.png" class="z-depth-0 img-fluid">
				</div>
				<!--/Grid column-->
				
				<!--Grid column-->
				<div class="col-2 text-center wow bounceIn" data-wow-delay="3s">
					<img src="images/carlesimo.png" class="z-depth-0 img-fluid">
				</div>
				<!--/Grid column-->
				
				<div class="col-1"> 
				</div>
			</div>
			<!--------->
			
			
						<!--Row Nomi-->
			<div class="row features-small mt-3 mb-5">
				<!--Grid column-->
				<div class="col-1">
				</div>
				<div class="col-2 wow fadeInUp" data-wow-delay="1s">
					<h5 class="feature-title font-weight-bold mb-1 text-center" id="alessandra">Alessandra Giordano</h5>
				</div>
				<!--/Grid column-->

				<!--Grid column-->
				<div class="col-2 wow fadeInUp" data-wow-delay="1.5s">
					<h5 class="feature-title font-weight-bold mb-1 text-center" id="alessandra">Andrea Castrataro</h5>
				</div>
				<!--/Grid column-->

				<!--Grid column-->
				<div class="col-2 wow fadeInUp" data-wow-delay="2s">
					<h5 class="feature-title font-weight-bold mb-1 text-center" id="alessandra">Loredana Lorefice</h5>
				</div>
				<!--/Grid column-->
				
				<!--Grid column-->
				<div class="col-2 wow fadeInUp" data-wow-delay="2.5s">
					<h5 class="feature-title font-weight-bold mb-1 text-center" id="alessandra">Tania Serfilippi</h5>
				</div>
				<!--/Grid column-->
				
				<!--Grid column-->
				<div class="col-2 wow fadeInUp" data-wow-delay="3s">
					<h5 class="feature-title font-weight-bold mb-1 text-center" id="alessandra">Silvana Carlesimo</h5>
				</div>
				<!--/Grid column-->
				
				<div class="col-1"> 
				</div>
			</div>
			<!--------->
		</section>
		<!--Section: More-->
</div>
	</main>
	<!--Main layout-->

	<!--Footer-->
	<footer class="page-footer text-center font-small mt-6 wow fadeIn">
	<!--Call to action-->
		<div class="pt-4">
			<p>Cooperativa Sociale Azzurra 84 - Via della Balduina 61 a/b 00136 ROMA</p>
			<p>Reg. Imprese Roma nr. CF 06619940585 - P.IVA 01582561005 - C.C.I.A.A. R.E.A. 534828</br>Iscr. Albo Reg. Coop. Soc. Sez. A nr. 127/63 bis e Sez. B nr. 17/63 - Iscr. Albo Nazionale Coop.Soc. A105185</p>
		</div>

		</div>
		<!--Copyright-->
		<div class="footer-copyright py-1">
			© 2019 Copyright: <a
				href="https://mdbootstrap.com/bootstrap-tutorial/" target="_blank">
				Isibit</a>
		</div>
		<!--/.Copyright-->

	</footer>
	<!--/.Footer-->

	<!-- SCRIPTS -->
	<!-- JQuery -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/jquery_coockies_1_4_1.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/mdb.min.js"></script>
		<!-- MDB core JavaScript -->
	<script type="text/javascript" src="js/wow.min.js"></script>
	<script type="text/javascript" src="js/cookie.js"></script>
	<!-- Google Analytics -->
	<script type="text/javascript" async src="https://www.googletagmanager.com/gtag/js?id=UA-136439000-1"></script>
	<!-- Initializations -->
	<script type="text/javascript">
    // Animations initialization
    new WOW().init();
  </script>
	<script>
	
  document.querySelectorAll('a[href^="#"]').forEach(anchor => {
	    anchor.addEventListener('click', function (e) {
	        e.preventDefault();

	        document.querySelector(this.getAttribute('href')).scrollIntoView({
	            behavior: 'smooth' });
	    });
	});

//Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight()+15,
      // All list items
      menuItems = topMenu.find("a"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+topMenuHeight;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems
           .parent().removeClass("active")
           .end().filter("[href='#"+id+"']").parent().addClass("active");
     }                   
  });
  
  </script>


</body>

</html>